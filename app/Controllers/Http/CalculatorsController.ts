import View from '@ioc:Adonis/Core/View'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class CalculatorsController {
  public async index (){
    return View.render('calculator.index')
  }

  public async calculator ({ request }: HttpContextContract){
    var params = request.only(['from','to','minutes','plan'])

    let from = parseInt(params.from)
    let to = parseInt(params.to)
    let minutes = parseInt(params.minutes)
    let plan = parseInt(params.plan)

    let withPlan = ((await this.calculatePlan({from, to, minutes, plan})) * 1.1).toFixed(2).replace('.',',')
    let withOutPlan = (await this.calculatePlan({from, to, minutes, plan:0})).toFixed(2).replace('.',',')

    let resp = {
      from,
      to,
      minutes,
      plan,
      withPlan:parseInt(withPlan) > 0 ? 'R$'+withPlan : '-',
      withOutPlan:parseInt(withPlan) > 0 ? 'R$'+withOutPlan : '-',
    }

    return resp
  }

  private async calculatePlan ({minutes,plan,to,from}){
    if(plan > 4) {
      return 0
    }
    let fee = 0
    plan *=30

    if(from === 11){
      switch(to){
        case 16: fee = ((minutes-plan) * 1.9); break
        case 17: fee = ((minutes-plan) * 1.7); break
        case 18: fee = ((minutes-plan) * 0.9); break
        default: return 0
      }
    }else{
      if(to === 11){
        switch(from){
          case 16: fee = ((minutes-plan) * 2.9); break
          case 17: fee = ((minutes-plan) * 2.7); break
          case 18: fee = ((minutes-plan) * 1.9); break
          default: return 0
        }
      }else{
        return 0
      }
    }
    return fee
  }
}
