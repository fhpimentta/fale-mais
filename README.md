<h3 align="center">
  Fale Mais
</h3>



## 📥 Instalação e execução

1. Faça um clone desse repositório.
2. Instalar <a href='https://nodejs.org/en/'>NodeJs</a>


### Backend

1. A partir da raiz do projeto Rode `yarn install` para instalar as dependências;
2. Rode `node ace serve --watch` para iniciar o servidor.



Feito com ♥ by [FernandoPimenta](https://www.linkedin.com/in/fepimenta/)
